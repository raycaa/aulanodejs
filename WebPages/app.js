// app.js
const express = require('express');
const path = require('path');
const app = express();
const port = 3000;

// Configurar o mecanismo de visualização
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

// Servir arquivos estáticos
app.use(express.static(path.join(__dirname, 'public')));
app.use('/bootstrap', express.static(path.join(__dirname, 'node_modules/bootstrap/dist')));

// Rotas
const indexRouter = require('./routes/index');
app.use('/', indexRouter);

// Iniciar o servidor
app.listen(port, () => console.log(`Servidor rodando em http://localhost:${port}`));
