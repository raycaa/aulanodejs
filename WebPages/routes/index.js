const express = require('express');
const router = express.Router();

router.get('/', (req, res) => res.render('home'));
router.get('/objetivo', (req, res) => res.render('objetivo'));
router.get('/desenvolvimento', (req, res) => res.render('desenvolvimento'));
router.get('/conclusao', (req, res) => res.render('conclusao'));

module.exports = router;