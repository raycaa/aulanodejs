const express = require('express');
const http = require('http');
const socketIO = require('socket.io');
const os = require('os');

const app = express();
const server = http.createServer(app);
const io = socketIO(server);

const getLocalIpAddress = () => {
  const interfaces = os.networkInterfaces();
  for (const key in interfaces) {
    for (const iface of interfaces[key]) {
      if (!iface.internal && iface.family === 'IPv4') {
        return iface.address;
      }
    }
  }
  return '127.0.0.1'; // fallback to localhost
};

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});

// ... (código anterior)

io.on('connection', (socket) => {
  console.log('Usuário conectado');

  // Solicita o nome do usuário ao se conectar
  socket.emit('request_username');

  // Recebe o nome do usuário do cliente
  socket.on('set_username', (username) => {
    console.log(`Usuário ${username} conectado`);

    // Define o nome de usuário e endereço IP para este socket
    socket.username = username;
    socket.ipAddress = socket.request.connection.remoteAddress;

    // Envia a mensagem de boas-vindas a todos os clientes conectados
    io.emit('chat message', { username: 'Servidor', message: `Bem-vindo, ${username}!` });

    // Atualiza a lista de usuários para todos os clientes
    updateUsersList();

    // Adiciona o usuário administrador à sala
    if (username === 'admin') {
      socket.join('admin-room');
    }
  });

  socket.on('disconnect', () => {
    if (socket.username) {
      console.log(`Usuário ${socket.username} desconectado`);

      // Atualiza a lista de usuários para todos os clientes
      updateUsersList();

      io.emit('chat message', { username: 'Servidor', message: `${socket.username} saiu do chat.` });
    } else {
      console.log('Usuário desconectado sem nome de usuário.');
    }
  });

  socket.on('disconnect_user', (usernameToDisconnect) => {
    const socketToDisconnect = Array.from(io.sockets.sockets.values()).find(
      (clientSocket) => clientSocket.username === usernameToDisconnect
    );

    if (socketToDisconnect) {
      socketToDisconnect.disconnect();
      console.log(`Usuário ${usernameToDisconnect} desconectado pelo administrador.`);
    } else {
      console.log(`Usuário ${usernameToDisconnect} não encontrado.`);
    }

    // Atualiza a lista de usuários para todos os clientes
    updateUsersList();
  });

  socket.on('chat message', (msg) => {
    console.log(`Mensagem recebida de ${socket.username}: ${msg}`);
    io.emit('chat message', { username: socket.username, message: msg });
  });

  // Função para atualizar a lista de usuários para todos os clientes
  const updateUsersList = () => {
    const users = [];
    io.sockets.sockets.forEach((clientSocket) => {
      if (clientSocket.username) {
        users.push({
          username: clientSocket.username,
          ipAddress: clientSocket.ipAddress,
        });
      }
    });
    io.emit('user_list', users);
  };
  // Função para desconectar um usuário (apenas permitido para o administrador)
  const disconnectUser = (username) => {
    if (socket.username === 'admin') {
      const socketToDisconnect = Array.from(io.sockets.sockets.values()).find(
        (clientSocket) => clientSocket.username === username
      );

      if (socketToDisconnect) {
        socketToDisconnect.disconnect();
        console.log(`Usuário ${username} desconectado pelo administrador.`);
      } else {
        console.log(`Usuário ${username} não encontrado.`);
      }

      // Atualiza a lista de usuários para todos os clientes
      updateUsersList();
    } else {
      console.log('Apenas o administrador pode desconectar usuários.');
    }
  };
});

// ... (código posterior)


server.listen(3000, () => {
  const ipAddress = getLocalIpAddress();
  console.log(`Servidor rodando em http://${ipAddress}:3000/`);
});